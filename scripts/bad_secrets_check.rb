table_column_combos = [
  [Namespace, "runners_token_encrypted"],
  [Project, "runners_token_encrypted"],
  [Ci::Build, "token_encrypted"],
  [Ci::Runner, "token_encrypted"],
  [ApplicationSetting, "runners_registration_token_encrypted"],
  [Group, "runners_token_encrypted"],
]
table_column_combos.each do |table,column|
  total = 0
  bad = []
  table.find_each do |data|
    begin
      total += 1
      ::Gitlab::CryptoHelper.aes256_gcm_decrypt(data[column])
    rescue => e
      bad << data
    end
  end
  puts "#{table.name}: #{bad.length} / #{total}"
end
