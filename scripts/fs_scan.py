#!/usr/bin/python

import os
from os.path import join, getsize

for root, dirs, files in os.walk('.'):
    print root, "consumes",
    print sum(getsize(join(root, name)) for name in files),
    print "bytes in", len(files), "non-directory files"
    for file in files:
       fileloc = root + "/" + file
       FILE = os.open(fileloc, os.O_RDONLY)
       junk = os.fstat(FILE)
       size = junk[6]
       atime = junk[7]
       mtime = junk[8]
       ctime = junk[9]
       uid = junk[4]
       gid = junk[5]
       print "   File: %s size: %s atime: %s mtime: %s ctime: %s" % (file,size,atime,mtime,ctime)
       os.close(FILE)

