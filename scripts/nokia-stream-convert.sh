#!/bin/bash
# Mencoder Script
# This version modified for use to transcode files to nokia n800 use
# Mark Waters - 20061007
#
#LOG="-passlogfile /tmp/nokia-divx2pass.log"
TMPF=$(tempfile -p 2pass)
LOG="-passlogfile $TMPF"

RESIZE="-sws 9 -vf scale=400:240"
#AUDIO="-oac mp3lam3 -lameopts cbr:br=96"
AUDIO="-oac lavc"
VIDEO="-ovc lavc -lavcopts"
VIDOPTS1="vcodec=mpeg4:vbitrate=400:autoaspect:vpass=1 -ffourcc DIVX -idx"
VIDOPTS2="vcodec=mpeg4:vbitrate=400:autoaspect:vpass=2 -ffourcc DIVX -idx"
SOURCE=$1
OUTPUT="${SOURCE%.mpg}.nokia.avi" # Change .ext to .nokia.avi
nice -n 15 mencoder $SOURCE $RESIZE $AUDIO $VIDEO $VIDOPTS1 $LOG -o /dev/null
nice -n 15 mencoder $SOURCE $RESIZE $AUDIO $VIDEO $VIDOPTS2 $LOG -o $OUTPUT

mv $OUTPUT /extra/videos/nokia/
rm $TMPF
